const express = require('express');
const https = require('https');
const fs = require('fs');
const open = require('open');

// haven't found a stdin module that works in WSL yet...
// const InputEvent = require('input-event');
// const input = new InputEvent('/dev/input/event0');
// const keyboard = new InputEvent.Keyboard(input);
// keyboard.on('keydown', (e) => { console.log(e); });

// const ioHook = require('iohook');
// ioHook.on('keydown', (e) => { console.log(e); });
// ioHook.start();

const RTCMultiConnectionServer = require('rtcmulticonnection-server');

const app = express();

app.use('/broadcast', express.static('./server/broadcast'));

const server = https.createServer({ key: fs.readFileSync('./server/privatekey.pem'), cert: fs.readFileSync('./server/certificate.pem') }, app);
const io = require('socket.io')(server, { cors: { origin: '*', credentials: true } });

io.on('connection', (socket) => {
  console.log('connection', socket.id);
  socket.on('keydown', (e) => { io.emit('keydown', e); });
  socket.on('keyup', (e) => { io.emit('keyup', e); });
  RTCMultiConnectionServer.addSocket(socket);
});

server.listen('8000', () => {
  console.log('server listening at https://localhost:8000');
  open('https://localhost:8000/broadcast');
});
