const connection = new RTCMultiConnection();
connection.socketMessageEvent = 'screen-sharing-demo'; // TODO: remove?
connection.session = { screen: true, oneway: true };
connection.enableLogs = false;
connection.open('desktop');

const screenEl = document.querySelector('.screen');
const streamEl = document.querySelector('.stream');

// TODO: capture stop and close share screen events here

connection.onstream = () => {
  streamEl.classList.remove('hidden');
  screenEl.classList.add('hidden');
};
