import io from 'socket.io-client';

// TODO: import this instead:
const CodecsHandler = (function () { function e(e, i) { const t = n(e); return t.videoCodecNumbers ? i === 'vp8' && t.vp8LineNumber === t.videoCodecNumbers[0] ? e : i === 'vp9' && t.vp9LineNumber === t.videoCodecNumbers[0] ? e : i === 'h264' && t.h264LineNumber === t.videoCodecNumbers[0] ? e : e = r(e, i, t) : e; } function r(e, r, n, i) { let t = ''; if (r === 'vp8') { if (!n.vp8LineNumber) return e; t = n.vp8LineNumber; } if (r === 'vp9') { if (!n.vp9LineNumber) return e; t = n.vp9LineNumber; } if (r === 'h264') { if (!n.h264LineNumber) return e; t = n.h264LineNumber; } let a = `${n.videoCodecNumbersOriginal.split('SAVPF')[0]}SAVPF `; let o = [t]; return i && (o = []), n.videoCodecNumbers.forEach((e) => { e !== t && o.push(e); }), a += o.join(' '), e = e.replace(n.videoCodecNumbersOriginal, a); } function n(e) { const r = {}; return e.split('\n').forEach((e) => { e.indexOf('m=video') === 0 && (r.videoCodecNumbers = [], e.split('SAVPF')[1].split(' ').forEach((n) => { (n = n.trim()) && n.length && (r.videoCodecNumbers.push(n), r.videoCodecNumbersOriginal = e); })), e.indexOf('VP8/90000') === -1 || r.vp8LineNumber || (r.vp8LineNumber = e.replace('a=rtpmap:', '').split(' ')[0]), e.indexOf('VP9/90000') === -1 || r.vp9LineNumber || (r.vp9LineNumber = e.replace('a=rtpmap:', '').split(' ')[0]), e.indexOf('H264/90000') === -1 || r.h264LineNumber || (r.h264LineNumber = e.replace('a=rtpmap:', '').split(' ')[0]); }), r; } function i(e, r, n) { return (function (e, r, n, i, t) { for (let a = n !== -1 ? n : e.length, o = r; o < a; ++o) if (e[o].indexOf(i) === 0 && (!t || e[o].toLowerCase().indexOf(t.toLowerCase()) !== -1)) return o; return null; }(e, 0, -1, r, n)); } function t(e) { const r = new RegExp('a=rtpmap:(\\d+) \\w+\\/\\d+'); const n = e.match(r); return n && n.length === 2 ? n[1] : null; } return { removeVPX(e) { const i = n(e); return e = r(e, 'vp9', i, !0), e = r(e, 'vp8', i, !0); }, disableNACK(e) { if (!e || typeof e !== 'string') throw 'Invalid arguments.'; return e = (e = (e = (e = e.replace('a=rtcp-fb:126 nack\r\n', '')).replace('a=rtcp-fb:126 nack pli\r\n', 'a=rtcp-fb:126 pli\r\n')).replace('a=rtcp-fb:97 nack\r\n', '')).replace('a=rtcp-fb:97 nack pli\r\n', 'a=rtcp-fb:97 pli\r\n'); }, prioritize(e, r) { if (r && r.getSenders && r.getSenders().length) { if (!e || typeof e !== 'string') throw 'Invalid arguments.'; r.getSenders().forEach((r) => { for (var n = r.getParameters(), i = 0; i < n.codecs.length; i++) if (n.codecs[i].mimeType == e) { n.codecs.unshift(n.codecs.splice(i, 1)); break; }r.setParameters(n); }); } }, removeNonG722(e) { return e.replace(/m=audio ([0-9]+) RTP\/SAVPF ([0-9 ]*)/g, 'm=audio $1 RTP/SAVPF 9'); }, setApplicationSpecificBandwidth(e, r, n) { return (function (e, r, n) { return r ? typeof isFirefox !== 'undefined' && isFirefox ? e : (n && (r.screen ? r.screen < 300 && console.warn('It seems that you are using wrong bandwidth value for screen. Screen sharing is expected to fail.') : console.warn('It seems that you are not using bandwidth for screen. Screen sharing is expected to fail.')), r.screen && n && (e = (e = e.replace(/b=AS([^\r\n]+\r\n)/g, '')).replace(/a=mid:video\r\n/g, `a=mid:video\r\nb=AS:${r.screen}\r\n`)), (r.audio || r.video) && (e = e.replace(/b=AS([^\r\n]+\r\n)/g, '')), r.audio && (e = e.replace(/a=mid:audio\r\n/g, `a=mid:audio\r\nb=AS:${r.audio}\r\n`)), r.screen ? e = e.replace(/a=mid:video\r\n/g, `a=mid:video\r\nb=AS:${r.screen}\r\n`) : r.video && (e = e.replace(/a=mid:video\r\n/g, `a=mid:video\r\nb=AS:${r.video}\r\n`)), e) : e; }(e, r, n)); }, setVideoBitrates(e, r) { return (function (e, r) { let n; const a = (r = r || {}).min; const o = r.max; const c = e.split('\r\n'); const u = i(c, 'a=rtpmap', 'VP8/90000'); if (u && (n = t(c[u])), !n) return e; let p; const d = i(c, 'a=rtpmap', 'rtx/90000'); if (d && (p = t(c[d])), !d) return e; const s = i(c, `a=fmtp:${p.toString()}`); if (s !== null) { let f = '\r\n'; f += `a=fmtp:${n} x-google-min-bitrate=${a || '228'}; x-google-max-bitrate=${o || '228'}`, c[s] = c[s].concat(f), e = c.join('\r\n'); } return e; }(e, r)); }, setOpusAttributes(e, r) { return (function (e, r) { r = r || {}; let n; const a = e.split('\r\n'); const o = i(a, 'a=rtpmap', 'opus/48000'); if (o && (n = t(a[o])), !n) return e; const c = i(a, `a=fmtp:${n.toString()}`); if (c === null) return e; let u = ''; return u += `; stereo=${void 0 !== r.stereo ? r.stereo : '1'}`, u += `; sprop-stereo=${void 0 !== r['sprop-stereo'] ? r['sprop-stereo'] : '1'}`, void 0 !== r.maxaveragebitrate && (u += `; maxaveragebitrate=${r.maxaveragebitrate || 1048576}`), void 0 !== r.maxplaybackrate && (u += `; maxplaybackrate=${r.maxplaybackrate || 1048576}`), void 0 !== r.cbr && (u += `; cbr=${void 0 !== r.cbr ? r.cbr : '1'}`), void 0 !== r.useinbandfec && (u += `; useinbandfec=${r.useinbandfec}`), void 0 !== r.usedtx && (u += `; usedtx=${r.usedtx}`), void 0 !== r.maxptime && (u += `\r\na=maxptime:${r.maxptime}`), a[c] = a[c].concat(u), e = a.join('\r\n'); }(e, r)); }, preferVP9(r) { return e(r, 'vp9'); }, preferCodec: e, forceStereoAudio(e) { for (var r = e.split('\r\n'), n = null, i = 0; i < r.length; i++) if (r[i].search('opus/48000') !== -1) { var t = extractSdp(r[i], /:(\d+) opus\/48000/i); break; } for (i = 0; i < r.length; i++) if (r[i].search('a=fmtp') !== -1 && extractSdp(r[i], /a=fmtp:(\d+)/) === t) { n = i; break; } return n === null ? e : (r[n] = r[n].concat('; stereo=1; sprop-stereo=1'), e = r.join('\r\n')); } }; }()); window.BandwidthHandler = CodecsHandler;

export default class BroadcastDesktop {
  constructor() {
    window.io = io;

    this.video = document.createElement('video');
    this.video.volume = 0;
    this.video.setAttribute('autoplay', true);
    this.video.setAttribute('playsinline', true);
    this.video.setAttribute('muted', true);

    const rtcScript = document.createElement('script');
    rtcScript.addEventListener('load', () => {
      this.connection = new window.RTCMultiConnection();
      this.connection.socketMessageEvent = 'screen-sharing-demo';
      this.connection.socketURL = ':8000/';
      this.connection.enableLogs = false;
      this.connection.session = { screen: true, oneway: true };
      this.connection.videosContainer = this.video;
      this.connection.processSdp = function (sdp) {
        // testing options to decrease latency
        sdp = CodecsHandler.disableNACK(sdp); // didn't see much difference with this
        sdp = CodecsHandler.setVideoBitrates(sdp, { min: 200000, max: 200000 }); // This helps a lot
        return sdp;
      };

      this.connection.join('desktop');
      // TODO: add a cancelable setinterval here in case client started before server as polling doesn't work

      const startButton = document.createElement('button');
      startButton.innerHTML = 'Waiting for Desktop...';
      startButton.className = 'stream-button disabled';
      startButton.style = 'position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); padding: 10px 20px; z-index: 1000;';
      startButton.addEventListener('click', () => {
        this.video.play();
        startButton.classList.add('hidden');
      });
      document.body.appendChild(startButton);

      this.connection.onstream = (event) => {
        event.mediaElement.removeAttribute('src');
        event.mediaElement.removeAttribute('srcObject');
        event.mediaElement.muted = true;
        event.mediaElement.volume = 0;
        this.video.srcObject = event.stream;
        startButton.innerHTML = 'Start Streaming &nbsp; →';
        startButton.classList.remove('disabled');
      };

      this.connection.onstreamended = (e) => {
        console.log('shutdown gracefully');
      // TODO maybe replace screentexture with "no remote" image
      };

      this.connection.onMediaError = (e) => {
        console.log('handle error');
      };
    });

    // TODO: host this file with express to keep everything "in house"
    rtcScript.src = 'https://cdn.jsdelivr.net/npm/rtcmulticonnection@3.7.0/dist/RTCMultiConnection.min.js';
    document.body.appendChild(rtcScript);

    // return this.video;
  }
}
