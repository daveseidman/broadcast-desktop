# Broadcast Desktop

Use WebRTC to broadcast your desktop for viewing within frontend applications (over LAN)  

## Usage

`npm install`  
`npm start`  


this should open a browser window on the machine you want to cast from and  
prompt you to pick the window you want to share.  

then in your front-end application:  

```
import BroadcastDesktop from 'broadcast-desktop/client';

const stream = new BroadcastDesktop();  <-- returns a video

document.body.appendChild(stream);

```

"Your connection is not private" issue

To view the screen inside your app on another machine on your LAN you'll need to
let chrome know you trust the URLs by visitng *BOTH* and clicking "Advanced" and
"proceed ..." otherwise your frontend app will error out when trying to signal the
socket server. Chrome Version (86.0.4240.111)   

example: your app is running on a machine with the IP 192.168.1.100, and you're testing
it on another computer or device on the same network. Visit https://192.168.1.100:8000/
on that machine. It'll be a dead link but Chrome will whitelist the domain so that way
when your app calls it Chrome knows it's safe.  

## Motivation

I needed a way to see my screen while working in VR and live-editing the scene.  
See example project here: https://gitlab.com/daveseidman/three-vr
